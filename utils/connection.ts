import { query, Request, Response } from "express";
import { createConnection, Connection } from "typeorm";

const pgp = require("pg-promise")();

//DOCKER CONTAINER
const username = "postgres";
const database = "strapiv7";
const password = "root";
const host = "192.168.3.100";
const port = 5432;

// Configuración de la conexión a PostgreSQL
const db = pgp({
  host: "192.168.3.100",
  user: "postgres",
  database: "strapiv5",
  password: "root",
  port: 5433,
});
// Configuración de la conexión a PostgreSQL
//Conexcion para Generar base de datos cuando se corre el projecto
const connectionConfig = {
  type: "postgres",
  host: "192.168.3.100",
  port: 5433,
  username: "postgres",
  password: "root",
};

const Pool = require("pg").Pool;
export const pool = new Pool({
  host: "192.168.3.100",
  user: "postgres",
  database,
  password: "root",
  port: 5433,
});
//Verificacion de base de datos cuando se corre el projecto
export const connectDataBase = async () => {
  const res = await pool.query(
    `SELECT datname FROM pg_catalog.pg_database WHERE datname = '${database}'`
  );

  if (res.rowCount === 0) {
    console.log(`${pool.database} database not found, creating it.`);
    await pool.query(`CREATE DATABASE "${database}";`);
    console.log(`created database ${database}`);
  } else {
    console.log(`${database} database exists.`);
  }
};

//Cuando se consume la api se verifica que la tabla a la que se hace la peticion exista
const isModelValid = async (tableName: string) => {
  try {
    await connectDataBase();
  } catch (error) {
    return false;
  }
  const result = await db.oneOrNone(
    `
  SELECT table_name
  FROM information_schema.tables
  WHERE table_schema = 'public' AND table_name = $1
`,
    [tableName]
  );

  pool.connect();
  //  si no existe se retorna falso
  return !!result;
};


// METODOS CRUD REUTILIZABLES PARA EL CONSUMO DE LA API

// OBTENER TODOS LOS DATOS
export const getAllData = async (request: Request, response: Response) => {
  const modelTable = request.params.model;
  if (!(await isModelValid(modelTable))) {
    return response.status(500).send({ message: "Invalid Model", data: [] });
  }
  try {
    const result = await pool.query(
      `SELECT * FROM ${modelTable} ORDER BY id ASC`
    );
    return response.send({ message: "All Data", data: result.rows });
  } catch (error) {}
  return response
    .status(500)
    .send({ message: "Get data with error", data: [] });
};

// BUSCAR PORR ID
export const findById = async (request: Request, response: Response) => {
  const modelTable = request.params.model;
  const modelTableId = request.params.id;
  if (!(await isModelValid(modelTable))) {
    return response.status(500).send({ message: "Invalid Model", data: [] });
  }
  try {
    const result = await pool.query(
      `SELECT * FROM ${modelTable} WHERE id = ${modelTableId}`
    );
    return response.send({ message: "Get Successfull", data: result.rows[0] });
  } catch (error) {}
  return response
    .status(500)
    .send({ message: "Get data with error", data: [] });
};

// GUARDAR O ACTUALIZAR, SEGUN CORRESPONDA EL ID
export const storeData = async (request: Request, response: Response) => {
  const modelTable = request.params.model;
  if (!(await isModelValid(modelTable))) {
    return response.status(500).send({ message: "Invalid Model", data: [] });
  }
  try {
    const modelData = request.body.data;
    const modelId = modelData.id;

    if (modelId >= 0) {
      delete modelData.id;
    }
    const keys = Object.keys(modelData);
    const data = Object.values(modelData);

    const querySave = `INSERT INTO ${modelTable} (${keys.join(
      ", "
    )}) VALUES(${data
      .map((_, index) => `$${index + 1}`)
      .join(", ")}) RETURNING *`;
    const queryUpdate = `UPDATE ${modelTable} SET ${keys
      .map((key, i) => `${key}=$${i + 1}`)
      .join(", ")} WHERE id=${modelId} RETURNING *`;
    const query = modelId > 0 ? queryUpdate : querySave;

    const result = await pool.query(query, data);

    return response.send({
      message: modelId > 0 ? "Updated Successfull" : "Saved Successfull",
      data: result.rows[0],
    });
  } catch (error) {}
  return response.status(500).send({ message: "Saved Failed", data: [] });
};

// ELIMINAR DATO POR ID
export const deleteById = async (request: Request, response: Response) => {
  const modelTable = request.params.model;
  const modelTableId = request.params.id;
  if (!(await isModelValid(modelTable))) {
    return response.status(500).send({ message: "Invalid Model", data: [] });
  }
  try {
    const result = await pool.query(
      `DELETE FROM ${modelTable} WHERE id = ${modelTableId}`
    );
    return response.send({
      message: "Deleted Successfull",
      data: result.rows[0],
    });
  } catch (error) {}
  return response.status(500).send({ message: "Deleted Failed", data: [] });
};
