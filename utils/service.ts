import { storeData } from "./connection"
import { Request, Response } from 'express'
  //CREACION DE INTERFACE PARA LA OBTENCION DE DATOS Y MAYOR CONTROL DE LA INFORMACION
interface ApiModel {
    name: {
      title: string
      first: string
      last: string
    }
    gender: string
    email: string
    phone: string
    dob: {
      date: Date
      age: number
    }
  }

  //CREACION DE INTERFACE PARA LA OBTENCION DE DATOS Y MAYOR CONTROL DE LA INFORMACION
interface Api {
    info: any
    results: ApiModel[]
}

//  CONSUMIMOS API Y GENERAMOS NUEVA RESPUESTA A PARTIR DE LA API DATA
export const getDataCloud = async (request: Request, response: Response) => {
  const cloudData: Api = await fetch(
    "https://randomuser.me/api/?results=200&inc=gender,name,email,dob,phone"
  ).then((response) => response ? response.json() : ({ results: [] }));

  const allDataCloud = cloudData.results.flat() as ApiModel[]

  const newData = allDataCloud.map((model: ApiModel) => ({
    name: `${model.name.first} ${model.name.last}`,
    email: model.email,
    age: model.dob.age,
    gender: model.gender === 'male' ? 'Masculino' : 'Femenino',
  }))
  response.json({ data: newData })

  // ACA SE PUEDE GENERAR LOGICA PARA GUARDAR LA INFORMACION DE LA API CONSUMIDA Y GUARDARLA EN NUESTRA BASE DE DATOS
  // NO CREADA POR FALTA DE TIEMPO
 /*  allDataCloud.map(async (entity) => {
    request.body.model = entity
    await storeData(request, response)
  }) */

};