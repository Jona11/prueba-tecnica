// TODAS LAS VARIABLES SE DECLARARON CON NOMBRE ENTENDIBLE PARA IDENTIFICAR FACILMENTE A QUE CORRESPONDEN 
const endPoint = "http://localhost:8000/api/data";
const table = document.querySelector("#table");
const btn = document.querySelector("#btnExport");
const total = document.querySelector("#total");
const totalMale = document.querySelector("#totalMale");
const totalFemale = document.querySelector("#totalFemale");
const classBase = "p-3 hover:bg-gray-100 cursor-pointer";
let allData = [];

// EXPORTDATOS DATOS A CSV USANDO JSPREADSHEET
const exportData = (data, columns) => {
    const div = document.createElement("div");
    div.setAttribute("id", "divCsv");

    // GENERATION DEL ARCHIVO
    const csvFile = jspreadsheet(div, {
        // SET HEADERS CSV
        includeHeadersOnDownload: true,
        csvFileName: "DatosDePersonas",
        data,
        columns,
        csvHeaders: true,
    });
    // INDICAR AL NAVEGADOR QUE LO DESCARGUE
    csvFile.download();
};

// FUNCTION REUTILIZABLE PARA REDUCIR CODIGO AL GENERAR LAS CELDAS DE LA TABLA
const generateDiv = (prop) => {
    const div = document.createElement("div");
    div.className = classBase;
    div.textContent = `${prop}`;
    return div;
};

// FUNCION PARA OBTENER LA INFORMACION AL ABRIR EL INDEX.HTML
const getData = async() => {
    const response = await fetch(endPoint, {
        method: "get",
    }).then((response) => (response ? response.json() : { data: [] }));

    const femaleTotal = response.data.filter(
        (item) => item.gender === "Femenino"
    );
    const maleTotal = response.data.filter((item) => item.gender === "Masculino");
    total.textContent = `${response.data.length}`;
    totalFemale.textContent = `${femaleTotal.length}`;
    totalMale.textContent = `${maleTotal.length}`;

    response.data.map((model) => {
        table.appendChild(generateDiv(model.name));
        table.appendChild(generateDiv(model.email));
        table.appendChild(generateDiv(model.gender));
        table.appendChild(generateDiv(model.age));
    });

    allData = response.data;
};
// EJECUCION DE LA  FUNCION PARA OBTENER LA INFORMACION AL ABRIR EL INDEX.HTML
getData();

// BOTON PARA EXPORTAR INFORMACION
btn.addEventListener("click", async() => {
    const columns = [
        { type: "text", title: "Name", width: 120 },
        { type: "text", title: "Correo", width: 120 },
        { type: "text", title: "Genero", width: 120 },
        { type: "number", title: "Edad", width: 120 },
    ];
    exportData(Object.values(allData), columns);
});