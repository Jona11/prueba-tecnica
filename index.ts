import express, {
  Express,
  Request,
  Response,
  Application,
} from "express";
import dotenv from "dotenv";
import { getAllData, findById, storeData, deleteById, connectDataBase } from "./utils/connection";
import { getDataCloud } from "./utils/service";
import path from 'path';
import cors from 'cors';
//For env File
const app: Application = express();
app.use(express.json());
app.use(cors());
dotenv.config();
const port = process.env.PORT || 8000;

app.get("/api/data", (req: Request, res: Response) => getDataCloud(req, res))
app.get("/", (req: Request, res: Response) => res.send('Servidor Levantado con Exito, Habra el archivo html '))

app.listen(port, () => {
  console.log(`Server running in http://localhost:${port}`);
});
